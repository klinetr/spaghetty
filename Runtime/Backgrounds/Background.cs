﻿using UnityEngine;
using UnityEngine.UI;

namespace Spaghetty
{
    public class Background : MonoBehaviour
    {
        [Header("Component References")]
        [SerializeField] BackgroundLibrary _backgroundLibrary = null;
        [SerializeField] DialogEvent _backgroundEvent = null;
        Image _image;
        void Awake() => _image = GetComponent<Image>();
        void OnEnable() => _backgroundEvent.OnRaised += ParseSetBackgroundEvent;
        void OnDisable()=>_backgroundEvent.OnRaised -= ParseSetBackgroundEvent;

        public void ParseSetBackgroundEvent(string[] args)
        {
            if(args.Length != 1)
                throw new System.ArgumentException($"Invalid number of args for Set Background Event.");
            SetBackground(args[0]);
        }

        public void SetBackground(string background)
        {
            if (_image == null) _image = GetComponent<Image>();

            foreach (BackgroundLibraryElement ble in _backgroundLibrary.elements)
            {
                if (ble.name == background)
                {
                    _image.sprite = ble.background;
                }
            }
        }
    }
}
