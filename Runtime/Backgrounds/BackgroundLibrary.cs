﻿using System.Collections.Generic;
using UnityEngine;

namespace Spaghetty
{
    [System.Serializable]
    public struct BackgroundLibraryElement
    {
        public string name;
        public Sprite background;
    };

    [CreateAssetMenu(fileName = "New Background Library", menuName = "Spaghetty/Backgrounds/Background Library", order = 1)]
    public class BackgroundLibrary : ScriptableObject
    {
        public List<BackgroundLibraryElement> elements;
        public Sprite this[string key] =>
                elements.Find((x) => x.name.TrimToLower() == key.TrimToLower()).background;
        public int Length => elements.Count;
    }
}
