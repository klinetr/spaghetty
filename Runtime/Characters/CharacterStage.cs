﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace Spaghetty
{
    public class CharacterStage : MonoBehaviour
    {
        StagePosition[] _positions = null;
        IEnumerable<StagePosition> Positions {
            get {
                if(_positions == null) _positions = GetComponentsInChildren<StagePosition>();
                return _positions;
            }
        }
        public Transform GetStagePosition(string name) =>
            Positions.Where(x => x.name.TrimToLower() == name.TrimToLower())
                    .Select(y => y.transform).FirstOrDefault();
    }
}