﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaghetty{
    public class DialogCharacter : MonoBehaviour
    {
        [SerializeField] CharacterMovementTypeLibrary _characterMovement = null;
        [SerializeField] CharacterStage _stage = null;
        [SerializeField] SpriteMask HighlightedEffect = null;
        [SerializeField] CharacterEmoteLibrary emotions = null;
        [SerializeField] AnimationCurve _bloopWiggleCurve = null;
        SpriteRenderer spriteRenderer;
        bool selected;
        bool hidden = false;

        void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        //Need to be able to pass movement type here. 
        //Movement type should probably contain all timing information as well as curve stuff
        //Best to define timing information as speed or time to complete movement??
        public void ShowCharacter(string[] args)
        {
            var emote = (args.Length > 0) ? args[0].TrimToLower() : "";
            var stagePosition = (args.Length > 1) ? args[1].TrimToLower() : "";
            var delay = (args.Length > 2) ? args[2].GetFloat() : 0.0f;
            if (stagePosition != null && stagePosition != "")
            {
                var _targetTransform = _stage.GetStagePosition(stagePosition);
                if(_targetTransform == null) {
                    Debug.LogWarning($"This character does not have a state position with name {stagePosition}");
                }else{
                    transform.SetPositionAndRotation(_targetTransform.position, _targetTransform.rotation);
                    transform.localScale = _targetTransform.localScale;
                }
            }
            spriteRenderer.enabled = true;

            IEnumerator waitToSetSprite()
            {
                yield return new WaitForSeconds(delay);
                SetSprite(emote);
            }

            StartCoroutine(waitToSetSprite());
        }

        public void MoveCharacter(string[] args)
        {
            var toStagePosition = (args.Length > 0) ? args[0].TrimToLower() : "";
            var animation = (args.Length > 1) ? args[1].TrimToLower() : "";
            var duration = (args.Length > 2) ? args[2].GetFloat() : 0.0f;
            if (toStagePosition != null && toStagePosition != "")
            {
                var _targetTransform = _stage.GetStagePosition(toStagePosition);
                if(_targetTransform == null) {
                    Debug.LogWarning($"This character does not have a state position with name {toStagePosition}");
                }else{
                    StartCoroutine(AnimateCharacter(animation, duration, _targetTransform));
                }
            }else{
                Debug.LogWarning($"Stage Position string was null or empty.");
            }
        }

        public void HideCharacter(string[] args)
        {
            var emote = (args.Length > 0) ? args[0].TrimToLower() : "";
            hidden = true;
            SetSprite(emote);
            HighlightedEffect?.gameObject.SetActive(false);
            selected = false;
        }

        public void FlipCharacter(string[] args)
        {
            var delay = (args.Length > 0) ? args[0].GetFloat() : 0f;
            IEnumerator waitToFlip()
            {
                yield return new WaitForSeconds(delay);
                spriteRenderer.flipX = !spriteRenderer.flipX;
                if (HighlightedEffect != null)
                {
                    HighlightedEffect.transform.localScale = (spriteRenderer.flipX) ?
                        Vector3.right * -2 + Vector3.one :
                        Vector3.one;
                    HighlightedEffect.transform.localPosition = 
                        Vector3.Scale(HighlightedEffect.transform.localPosition, Vector3.left);
                }
            }
            if(selected) StartCoroutine(BloopCharacter());
            StartCoroutine(waitToFlip());
        }

        IEnumerator DoHighlight(float delay, bool value)
        {
            yield return new WaitForSeconds(delay);
            if (value && !hidden && spriteRenderer.enabled) HighlightedEffect?.gameObject.SetActive(value);
        }

        IEnumerator AnimateCharacter(string animation, float duration, Transform to)
        {
            CharacterMovementTypeAsset movement = _characterMovement[animation];
            float timer = 0;
            Quaternion _startRotation = transform.rotation;
            Vector3 _startPos = transform.position;
            while(timer < duration)
            {
                float x = Mathf.Lerp(_startPos.x,
                    to.position.x,
                    movement.XPosition.Evaluate(timer / duration));
                float y = Mathf.Lerp(_startPos.y,
                    to.position.y,
                    movement.YPosition.Evaluate(timer / duration));
                transform.position = new Vector3(x, y, transform.position.z);
                transform.rotation = Quaternion.Lerp(_startRotation, to.rotation, timer / duration);
                timer += Time.deltaTime;
                yield return null;
            }
        }

        public void SetSprite(string emote)
        {
            if(emotions.ContainsKey(emote))
            {
                
                if(selected) StartCoroutine(BloopCharacter());
                spriteRenderer.sprite = emotions[emote];
                if (HighlightedEffect != null && spriteRenderer.enabled)
                {
                    HighlightedEffect.sprite = emotions[emote];
                }
            }
            else
            {
                Debug.LogWarning(emote + " emote for character " + name + " not found.");
            }
        }

        public void SelectCharacter()
        {
            if (gameObject.activeSelf && !selected)
            {
                spriteRenderer.color = Color.white;
                StartCoroutine(DoHighlight(0.0f, true));
                selected = true;
                StartCoroutine(BloopCharacter());
            }
        }

        public void DeselectCharacter(Color fadeColor)
        {
            if (gameObject.activeSelf)
            {
                selected = false;
                spriteRenderer.color = fadeColor;
                if (HighlightedEffect != null)
                {
                    HighlightedEffect.gameObject.SetActive(false);
                }
            }
        }

        IEnumerator BloopCharacter()
        {
            float timer = 0;
            Vector3 _startScale = transform.localScale;
            float duration = 0.2f;
            while(timer <= duration)
            {
                timer += Time.deltaTime;
                yield return null;
                transform.localScale = new Vector3(
                    _startScale.x * (_bloopWiggleCurve.Evaluate(timer / duration)),
                    _startScale.y * (_bloopWiggleCurve.Evaluate(timer / duration)),
                    _startScale.z
                );
            }
        }
    }
}