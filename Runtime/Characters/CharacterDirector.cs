﻿using UnityEngine;
using System.Linq;

namespace Spaghetty
{
    public class CharacterDirector : MonoBehaviour
    {
        public Color spriteFadeColor;
        private DialogCharacter[] _characters = null;
        [SerializeField] private DisplayLineEvent _displayLine = default;
        [SerializeField] private DialogEvent _showCharacterEvent = default;
        [SerializeField] private DialogEvent _hideCharacterEvent = default;
        [SerializeField] private DialogEvent _flipCharacterEvent = default;
        [SerializeField] private DialogEvent _moveCharacterEvent = default;
        [SerializeField] private DialogCharacter _emptyCharacter = default;

        DialogCharacter[] Characters 
        {
            get{
                if(_characters == null)
                {
                    _characters = FindObjectsOfType<DialogCharacter>();
                }
                return _characters;
            }
        }

        void OnEnable()
        {
            _showCharacterEvent.OnRaised += ShowCharacter;
            _hideCharacterEvent.OnRaised += HideCharacter;
            _flipCharacterEvent.OnRaised += FlipCharacter;
            _moveCharacterEvent.OnRaised += MoveCharacter;
            _displayLine.OnRaised += DisplayLine;
        }

        void OnDisable()
        {
            _showCharacterEvent.OnRaised -= ShowCharacter;
            _hideCharacterEvent.OnRaised -= HideCharacter;
            _flipCharacterEvent.OnRaised -= FlipCharacter;
            _moveCharacterEvent.OnRaised -= MoveCharacter;
            _displayLine.OnRaised -= DisplayLine;
        }

        public void ShowCharacter(string[] args)
        {
            var character = GetCharacter(args[0].TrimToLower());
            character?.ShowCharacter(args.Skip(1).ToArray());
        }

        public void HideCharacter(string[] args)
        {
            var character = GetCharacter(args[0].TrimToLower());
            character?.HideCharacter(args.Skip(1).ToArray());
        }

        public void MoveCharacter(string[] args)
        {
            var character = GetCharacter(args[0].TrimToLower());
            character?.MoveCharacter(args.Skip(1).ToArray());
        }

        public void FlipCharacter(string[] args)
        {
            var character = GetCharacter(args[0]);
            character?.FlipCharacter(args.Skip(1).ToArray());
        }

        void DisplayLine(DialogLine args) =>
            HighlightCharacter(args.character);

        public void HighlightCharacter(string name)
        {
            var character = GetCharacter(name);
            if(character == _emptyCharacter) return;
            character?.SelectCharacter();
            foreach (var otherCharacter in Characters)
            {
                if (character != otherCharacter)
                {
                    otherCharacter.DeselectCharacter(spriteFadeColor);
                }
            }
        }

        DialogCharacter GetCharacter(string name)
        {
           if(name == null || name == "") return _emptyCharacter;
            return Characters
                .Where(x => x.name.ToLower() == name.ToLower())
                .FirstOrDefault();
        }
    }
}