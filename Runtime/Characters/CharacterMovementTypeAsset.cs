﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaghetty
{
    [CreateAssetMenu(fileName = "New Character Movement Type", menuName = "Spaghetty/Characters/Character Movement Type", order = 1)]
    public class CharacterMovementTypeAsset : ScriptableObject
    {
        [SerializeField] AnimationCurve xPosition = null;
        [SerializeField] AnimationCurve yPosition = null;

        public AnimationCurve XPosition => xPosition;
        public AnimationCurve YPosition => yPosition;
    }
}
