﻿using UnityEngine;
namespace Spaghetty
{
    [CreateAssetMenu(fileName = "New Character Emote", menuName = "Spaghetty/Characters/Character Emote", order = 1)]
    public class CharacterEmoteAsset : ScriptableObject
    {
        [SerializeField] string _emoteName = default;
        [SerializeField] Sprite _emote = null;
        public Sprite Emote => _emote;
        public string EmoteName => _emoteName;
    }
}
