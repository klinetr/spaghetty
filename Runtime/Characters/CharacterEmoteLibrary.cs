﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Spaghetty 
{
    [CreateAssetMenu(fileName = "New Character Emote Library", menuName = "Spaghetty/Characters/Character Emote Library", order = 1)]
    public class CharacterEmoteLibrary : ScriptableObject
    {
        [SerializeField] CharacterEmoteAsset[] emotions = null;
        Dictionary<string, Sprite> emotionDict;
        void Awake() => Init();

        void Init()
        {
            if (emotionDict == null)
            {
                emotionDict = new Dictionary<string, Sprite>();
                foreach (CharacterEmoteAsset c in emotions)
                {
                    emotionDict.Add(c.EmoteName.TrimToLower(), c.Emote);
                }
            }
        }

        public Sprite this[string name]
        {
            get
            {
                Init();
                return emotionDict[name.TrimToLower()];
            }
        }

        public int Length
        {
            get
            {
                Init();
                return emotionDict.Count;
            }
        }

        public bool ContainsKey(string key)
        {
            Init();
            return emotionDict.ContainsKey(key.TrimToLower());
        }

        public IEnumerable<CharacterEmoteAsset> AllEmotes => emotions;
    }
}
