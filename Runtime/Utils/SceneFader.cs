﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New Scene Fader", menuName = "Scenes/Create New Scene Fader", order = 1)]
public class SceneFader : ScriptableObject
{
    [SerializeField] GameObject faderPrefab = null;
    [SerializeField] float fadeInTime = 1;
    [SerializeField] float fadeOutTime = 1;

    Animator _faderAnimator = null;
    GameObject _faderObject;

    //External binding, summons a fader to do its thing
    public IEnumerator LoadSceneAsync(string sceneName)
    {
        yield return LoadSceneRoutine(sceneName);
    }

    IEnumerator LoadSceneRoutine(string sceneName)
    {
        yield return FadeOutScene(sceneName);
        yield return FadeInScene();
    }

    public IEnumerator FadeOutScene(string sceneName)
    {
        _faderObject = Instantiate(faderPrefab);
        _faderAnimator = _faderObject.GetComponentInChildren<Animator>();
        DontDestroyOnLoad( _faderObject);
        float timer = 0;
        while (timer < fadeInTime)
        {
            timer += Time.deltaTime;
            _faderAnimator.SetFloat("normalizedTime", timer / fadeInTime);
            yield return null;
        }

        _faderAnimator.SetFloat("normalizedTime", 1);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public IEnumerator FadeInScene()
    {
        float timer = fadeOutTime;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            _faderAnimator.SetFloat("normalizedTime", timer / fadeOutTime);
            yield return null;
        }
        Destroy(_faderObject);
    }
}
