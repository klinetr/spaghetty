﻿using System.Collections.Generic;
using UnityEngine;
//I CANT SERIALIZE YOOOOOUUUU No
public class Library<TKey, TValue> : ScriptableObject
{
    [System.Serializable]
    class LibraryElement : Dictionary<TKey, TValue> {}
    LibraryElement elements;
    public TValue this[TKey key] => elements[key];
    public int Length => elements.Count;
    public int Count => elements.Count;
    public bool ContainsKey(TKey key) => elements.ContainsKey(key);
    public bool ContainsValue(TValue value) => elements.ContainsValue(value);
}
