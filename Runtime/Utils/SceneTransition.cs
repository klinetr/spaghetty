﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "New Scene Transition", menuName = "Scenes/Create New Scene Transition", order = 1)]
public class SceneTransition : ScriptableObject
{
    [SerializeField] SceneReference scene = null;
    [SerializeField] SceneFader _transition = null;

    public IEnumerator LoadSceneAsync()
    {
        yield return _transition.LoadSceneAsync(scene);
    }

    public IEnumerator FadeSceneOutAndLoadAsync()
    {
        yield return _transition.FadeOutScene(scene);
    }

    public IEnumerator FadeSceneInAsync()
    {
        yield return _transition.FadeInScene();
    }
}
