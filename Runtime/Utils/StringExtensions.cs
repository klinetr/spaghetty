﻿public static class StringExtensions
{
    public static string TrimToLower(this string s) =>
        s.Trim().ToLower();

    public static float GetFloat(this string s) {
        float result;
        if(float.TryParse(s.TrimToLower(), out result))
        {
            return result;
        }else{
            throw new System.ArgumentException($"Yo {s} is not a float");
        }
    }
}