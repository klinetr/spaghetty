﻿using System.Collections.Generic;
using UnityEngine;

namespace Spaghetty
{
    [System.Serializable]
    public struct SceneLibraryElement
    {
        public string name;
        public SceneTransition transition;
    }

    [CreateAssetMenu(fileName = "New Scene Library", menuName = "PPP/Scenes/Scene Library", order = 1)]
    public class SceneLibrary : ScriptableObject
    {
        public List<SceneLibraryElement> scenes;
        public SceneTransition this[string key] =>
            scenes.Find((x) => x.name.TrimToLower() == key.TrimToLower()).transition;
        public int Length => scenes.Count;
    }
}
