﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

namespace Spaghetty
{
    public struct ChoiceData
    {
        public string[] args;
        public int index;
    }

    public struct ChoiceStartedEventArgs
    {
        public ChoiceData[] Choices;
    }


    [CreateAssetMenu(fileName = "New Dialog Choice Type", menuName = "Spaghetty/Dialog Choice Type", order = 1)]
    public class DialogChoiceType : ScriptableObject
    {
        void OnEnable() => hideFlags = HideFlags.DontUnloadUnusedAsset;
        public List<string> aliases;
        public event Action<ChoiceStartedEventArgs> OnStarted = null;
        public event Action<int> OnFinished = null;
        public bool CheckAliases(string id)
        {
            return id.ToLower().Trim() == name.ToLower().Trim() || 
                aliases.Select(x => x.ToLower().Trim()).ToList().Contains(id.ToLower().Trim());
        }

        public void StartChoice(ChoiceStartedEventArgs args) => OnStarted?.Invoke(args);
        public void FinishChoice(int choiceIndex) => OnFinished?.Invoke(choiceIndex);
    }
}