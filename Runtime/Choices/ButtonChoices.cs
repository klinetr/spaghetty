﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;
using TMPro;

namespace Spaghetty
{
    public class ButtonChoices : MonoBehaviour
    {
        public GameObject dialogChoicePrefab;
        private List<DialogChoiceButton> dialogChoices = new List<DialogChoiceButton>();
        [SerializeField] Transform[] _buttonParents = null;
        [SerializeField] DialogChoiceType _dialogChoiceType = null;
        [SerializeField] GameObject _questionBox = null;
        TextMeshProUGUI _questionBoxText = null;

        void Awake()
        {
            _questionBoxText = _questionBox?.GetComponentInChildren<TextMeshProUGUI>(true);
        }

        void OnEnable()
        {
            _dialogChoiceType.OnStarted += SpawnButtons;
        }

        void OnDisable()
        {
            _dialogChoiceType.OnStarted -= SpawnButtons;
        }

        public void SpawnButtons(ChoiceStartedEventArgs choiceArgs)
        {
            ClearButtons();
            if(_questionBoxText != null)
            {
                _questionBoxText.text = choiceArgs.Choices[0].args[0];
            }

            IEnumerator SpawnButtonsRoutine()
            {
                _questionBox?.SetActive(true);
                yield return new WaitForSeconds(0.3f);

                foreach (ChoiceData choice in choiceArgs.Choices)
                {
                    GameObject choiceButton = Instantiate(dialogChoicePrefab, GetNextAvailableParent());
                    DialogChoiceButton dChoice = choiceButton.GetComponent<DialogChoiceButton>();
                    dChoice.ShowText(choice.args[choice.args.Length - 1]);
                    dChoice.AddListenerOnClick(() => ChooseChoiceIndex(choice.index));
                    dialogChoices.Add(dChoice);
                    yield return new WaitForSeconds(0.2f);
                }
            }
            StartCoroutine(SpawnButtonsRoutine());
        }

        public Transform GetNextAvailableParent() =>
            _buttonParents.Where(x => x.childCount == 0).FirstOrDefault();

        void ChooseChoiceIndex(int index)
        {
            ClearButtons();
            _dialogChoiceType.FinishChoice(index);
        }

        void ClearButtons()
        {
            dialogChoices.ForEach(g => Destroy(g.gameObject));
            dialogChoices.Clear();
            _questionBox?.SetActive(false);
        }
    }
}
