﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace Spaghetty
{
    public class DialogChoiceButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] GameObject _effect = null;
        GameObject _textObject = null;

        TextMeshProUGUI text;
        float _targetScale = 1;
        float _bubbleTargetScale = 1;
        Vector3 _startScale, _bubbleStartScale, velocity;
        
        void Awake()
        {
            text = GetComponentInChildren<TextMeshProUGUI>();
            _textObject = text.gameObject;
            _startScale = _textObject.transform.localScale;
            _bubbleStartScale = transform.localScale;
        }

        public void ShowText(string _optionText)
        {
            text.text = _optionText;
        }

        public void AddListenerOnClick(UnityAction action)
        {
            GetComponent<Button>().onClick.AddListener(action);
        }

        public void OnPointerEnter(PointerEventData pointerEventData)
        {
            _effect?.SetActive(true);
            _targetScale = 1.2f;
            _bubbleTargetScale = 1.1f;
        }

        public void OnPointerExit(PointerEventData pointerEventData)
        {
            _effect?.SetActive(false);
            _targetScale = 1f;
            _bubbleTargetScale = 1f;
        }

        void Update()
        {
            _textObject.transform.localScale = Vector3.SmoothDamp(_textObject.transform.localScale,
                _startScale * _targetScale, ref velocity, 0.2f);
            transform.localScale = Vector3.SmoothDamp(transform.localScale,
                _bubbleStartScale * _bubbleTargetScale, ref velocity, 0.3f);
        }
    }
}
