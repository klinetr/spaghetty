﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaghetty 
{
    [CreateAssetMenu(fileName = "New Dialog Choice Type Library", menuName = "Spaghetty/Dialog Choice Type Library", order = 1)]
    public class DialogChoiceTypeLibrary : ScriptableObject
    {
        void OnEnable() => hideFlags = HideFlags.DontUnloadUnusedAsset;
        [SerializeField] private List<DialogChoiceType> _events = null;
        public IEnumerable<DialogChoiceType> Events => _events;
        public void StartDialogChoice(string id, ChoiceStartedEventArgs args)
        {
            DialogChoiceType eventToInvoke = _events.Find(x => x.CheckAliases(id));
            if (eventToInvoke == null)
            {
                throw new System.ArgumentException($"DialogChoiceType named {id} not found in dispatcher {name}");
            }
            eventToInvoke?.StartChoice(args);
        }
    }
}