﻿using System.Collections.Generic;
using UnityEngine;

namespace Spaghetty
{
    [System.Serializable]
    public struct MusicLibraryElement
    {
        public string name;
        public AudioClip clip;
    }

    [CreateAssetMenu(fileName = "New Music Library", menuName = "Spaghetty/Audio/Music Library", order = 1)]
    public class MusicLibrary : ScriptableObject
    {
        public List<MusicLibraryElement> music;
        public AudioClip this[string key] =>
            music.Find((x) => x.name.TrimToLower() == key.TrimToLower()).clip;
        public int Length => music.Count;
    }
}
