﻿using UnityEngine;
using System;

namespace Spaghetty
{
    [CreateAssetMenu(fileName = "New Display Line Event", menuName = "Spagetty/Display Line Event", order = 1)]
    public class DisplayLineEvent : ScriptableObject
    {
        void OnEnable() => hideFlags = HideFlags.DontUnloadUnusedAsset;
        public event Action<DialogLine> OnRaised = null;
        public void Invoke(DialogLine args) => OnRaised?.Invoke(args);
    }
}
