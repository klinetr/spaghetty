﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Spaghetty
{
    [CreateAssetMenu(fileName = "New Dialog Event", menuName = "Spagetty/Dialog Event", order = 1)]
    public class DialogEvent : ScriptableObject
    {
        void OnEnable() => hideFlags = HideFlags.DontUnloadUnusedAsset;
        public List<string> aliases;
        public event Action<string[]> OnRaised = null;
        public bool CheckAliases(string id) =>
            id.ToLower().Trim() == name.ToLower().Trim() || 
                aliases.Select(x => x.ToLower().Trim()).ToList().Contains(id.ToLower().Trim());
        public void Invoke(params string[] args) => OnRaised?.Invoke(args);
    }
}