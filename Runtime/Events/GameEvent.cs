﻿using UnityEngine;
using System;

namespace Spaghetty
{
    [CreateAssetMenu(fileName = "New Game Event", menuName = "Spagetty/Game Event", order = 1)]
    public class GameEvent : ScriptableObject
    {
        void OnEnable() => hideFlags = HideFlags.DontUnloadUnusedAsset;
        public event Action OnRaised = null;
        public void Invoke() => OnRaised?.Invoke();
    }
}
