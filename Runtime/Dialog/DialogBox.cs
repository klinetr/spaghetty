﻿using UnityEngine;
using UnityEngine.UI;
using System.Globalization;
using TMPro;
using RedBlueGames.Tools.TextTyper;

namespace Spaghetty
{
    [RequireComponent(typeof(Button))]
    public class DialogBox : DialogBoxBase
    {
        [SerializeField] AudioClip printSoundEffect = null;
        [SerializeField] TextMeshProUGUI nameText = null;
        [SerializeField] bool keepText = false;
        [SerializeField] bool isDefault = false;
        [SerializeField] bool clickAnywhere = false;
        [SerializeField] bool hideOnAwake = false;
        [SerializeField] TextTyper _typer = null;
        [SerializeField] GameEvent _readyToContinueEvent = null;
        TMP_Text m_TextComponent;
        Coroutine colorFadeRoutine;
        bool animText;
        float timer = 0;
        float nextContinueTime = 0.5f;
        bool _dialogBoxButtonWasClicked = false;

        // Creates a TextInfo based on the "en-US" culture.
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        public override bool IsTyping => _typer.IsTyping;
        public override bool IsDefault => isDefault;
        public override bool ClickAnywhere => clickAnywhere;
        public override void Finish() => _typer.Skip();
        public override void SetActiveState(bool state) => gameObject.SetActive(keepText || state);
        public override void DisplayLine(DialogLine storyLine)
        {
            if (nameText != null && storyLine.character != null)
            {
                nameText.gameObject.SetActive(true);
                nameText.text = textInfo.ToTitleCase(storyLine.character.ToLower());
            }
            else if (storyLine.character == null)
            {
                nameText.gameObject.SetActive(false);
            }

            if (gameObject.activeInHierarchy && storyLine.line.Length > 0)
                _typer.TypeText(storyLine.line);
            timer = nextContinueTime;
        }
        void HandleCharacterPrinted(string printedCharacter)
        {
            // Do not play a sound for whitespace
            if (printedCharacter == " " || printedCharacter == "\n") return;

            var audioSource = this.GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = this.gameObject.AddComponent<AudioSource>();
            }

            audioSource.clip = this.printSoundEffect;
            audioSource.Play();
        }

        void Awake()
        {
            var b = GetComponent<Button>();
            b?.onClick.AddListener(FireOnButtonClicked);
            _typer.CharacterPrinted.AddListener(HandleCharacterPrinted);
            if (!keepText) _typer.TypeText(" ");
            if (hideOnAwake) gameObject.SetActive(false);
        }

        void Update()
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)))
                {
                    if (IsTyping)
                    {
                        Finish();
                    }
                    else if (Input.GetKeyDown(KeyCode.Space) || ClickAnywhere || _dialogBoxButtonWasClicked)
                    {
                        _dialogBoxButtonWasClicked = false;
                        timer = nextContinueTime;
                        _readyToContinueEvent?.Invoke();
                    }
                }
            }
        }
    }
}