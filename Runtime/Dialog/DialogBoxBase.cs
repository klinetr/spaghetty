﻿using UnityEngine;

namespace Spaghetty{
    public abstract class DialogBoxBase : MonoBehaviour
    {
        public abstract void Finish();
        public abstract void DisplayLine(DialogLine storyLine);
        public abstract void SetActiveState(bool state);
        public abstract bool IsDefault {get;}
        public abstract bool IsTyping {get;}
        public abstract bool ClickAnywhere {get;}
        public event System.Action<DialogBoxBase> OnButtonClicked = null;
        protected void FireOnButtonClicked()
        {
            if(!IsTyping){
                OnButtonClicked?.Invoke(this);
            }
        }
    }
}