﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Spaghetty
{
    public class DialogBoxController : MonoBehaviour
    {
        [SerializeField] DialogEvent _changeDialogBox = default;
        [SerializeField] DialogEvent _hideDialogBox = default;
        [SerializeField] DisplayLineEvent _displayLineEvent = null;
        List<DialogBoxBase> dialogBoxes;
        DialogBoxBase selectedDialogBox;
        [SerializeField] DialogBox _narratorBox;

        void Awake()
        {
            dialogBoxes = new List<DialogBoxBase>(FindObjectsOfType<DialogBoxBase>());
        }

        void OnEnable()
        {
            _displayLineEvent.OnRaised += DisplayLine;
            _changeDialogBox.OnRaised += ChangeDialogBox;
            _hideDialogBox.OnRaised += HideDialogBox;

            foreach (var dbox in dialogBoxes)
            {
                if (dbox.IsDefault) selectedDialogBox = dbox;
            }
        }

        void OnDisable()
        {
            _displayLineEvent.OnRaised -= DisplayLine;
            _changeDialogBox.OnRaised -= ChangeDialogBox;
            _hideDialogBox.OnRaised -= HideDialogBox;
        }

        void HideDialogBox(string[] args)
        {
            selectedDialogBox.SetActiveState(false);
        }

        void DisplayLine(DialogLine line)
        {
            if(line.character == null)
            {
                selectedDialogBox.SetActiveState(false);
                _narratorBox.SetActiveState(true);
                _narratorBox.DisplayLine(line);
            }
            else
            {
                _narratorBox.SetActiveState(false);
                selectedDialogBox.SetActiveState(true);
                selectedDialogBox.DisplayLine(line);
            }
        }

        void ChangeDialogBox(string[] args) 
        {
            selectedDialogBox.SetActiveState(false);
            selectedDialogBox = dialogBoxes
                    .Where(x => x.name.Trim().ToLower() == args[0].Trim().ToLower())
                    .FirstOrDefault();
            selectedDialogBox.SetActiveState(true);
        }
    }
}
