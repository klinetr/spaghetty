﻿using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using System;
using System.Linq;
using System.Collections;

namespace Spaghetty
{
    public sealed class DialogDirector : ScriptableObject
    {
        [SerializeField] TextAsset _inkJSONAsset = null;
        [SerializeField] DisplayLineEvent _displayLine = null;
        [SerializeField] DialogEventLibrary _tagEventsLibrary = null;
        [SerializeField] DialogChoiceTypeLibrary _choiceTypesLibrary = null;
        [SerializeField] DialogEvent _waitEvent = default;
        [SerializeField] DialogEvent _changeSceneEvent = default;
        [SerializeField] DialogEvent _hideDialogBox = default;
        [SerializeField] GameEvent _readyToContinueEvent = null;
        [SerializeField] SceneLibrary _scenes = default;

        static Story story;
        string currentDialogText;
        float waitToContinueTimer = 0;
        bool isWaitingToContinue = false;
        bool _nextContinueJustShowsChoices = false;

        void Continue()
        {
            if(story == null) story = new Story(_inkJSONAsset.text);
           
            if (_nextContinueJustShowsChoices)
            {
                _hideDialogBox?.Invoke();
                HandleChoices();
                _nextContinueJustShowsChoices = false;
            }
            else if (story.canContinue)
            {
                currentDialogText = story.Continue();

                HandleTagsSOEvents();

                if (story.currentChoices.Count > 0)
                {
                    _nextContinueJustShowsChoices = true;
                }

                if (!isWaitingToContinue)
                {
                    FinishContinue();
                }
            }
        }

        void FinishContinue()
        {
            HandleDialog();
        }

        void HandleTagsSOEvents() => ParseTags(story.currentTags);

        void ParseTags(List<string> tags)
        {

            //If there is a change scene event, we must parse it first.
            var changeSceneEvent = tags.Find(x => x.TrimToLower().Contains("changeScene"));
            if (changeSceneEvent != null)
            {
                List<string> tokens = new List<string>(changeSceneEvent.Trim().Split(':'));
                _tagEventsLibrary.InvokeEvent(tokens[0].Trim().ToLower(),
                    tokens.GetRange(1, tokens.Count - 1).ToArray());
                //We do the rest once the next scene is loaded.
                return;
            }
            foreach (string inkTag in tags)
            {
                List<string> tokens = new List<string>(inkTag.Trim().Split(':'));
                _tagEventsLibrary.InvokeEvent(tokens[0].Trim().ToLower(),
                    tokens.GetRange(1, tokens.Count - 1).ToArray());
            }
        }

        ///I hate this nasty ass function
        void HandleDialog()
        {
            DialogLine dLine = new DialogLine();

            //---TODO What the fuck is this
            string[] tokens = currentDialogText.Trim().Split(':');
            List<string> tempTokens = new List<string>();

            for (int i = 0; i < tokens.Length; i++)
            {
                tempTokens.Add(tokens[i].Trim());
            }

            tokens = tempTokens.ToArray();
            //----

            switch (tokens.Length)
            {
                case 4:
                    dLine.character = tokens[1];
                    dLine.emote = tokens[2];
                    dLine.line = tokens[3];
                    break;
                case 3:
                    dLine.character = tokens[0];
                    dLine.emote = tokens[1];
                    dLine.line = tokens[2];
                    break;
                case 2:
                    dLine.character = tokens[0];
                    dLine.line = tokens[1];
                    break;
                case 1:
                    dLine.line = tokens[0];
                    break;
            }

            if (dLine.line == "EMPTY_LINE")
            {
                Continue();
            }
            else
            {
                _displayLine?.Invoke(dLine);
            }
        }

        void HandleChoices()
        {
            string choiceType = story.currentChoices[0].text.Trim().Split(':')[0];
            List<ChoiceData> choiceDatas = new List<ChoiceData>();

            foreach (Choice choice in story.currentChoices)
            {
                List<string> tokens = new List<string>(choice.text.Trim().Split(':'));

                tokens.RemoveAt(0); //Remove the name of the choicetype

                choiceDatas.Add(new ChoiceData
                {
                    index = choice.index,
                    args = tokens.ToArray()
                });
            }
            _choiceTypesLibrary.StartDialogChoice(choiceType, 
                new ChoiceStartedEventArgs{
                    Choices = choiceDatas.ToArray()
                });
        }

        void WaitForSeconds(string[] args)
        {
            isWaitingToContinue = true;
            waitToContinueTimer = float.Parse(args[0]);
            var g = new GameObject();
            DontDestroyOnLoad(g);
            var binding = g.AddComponent<CoroutineBinding>();
            binding.StartCoroutine(Wait(binding));
        }

        void OnSceneChangeEvent(string[] args)
        {
            isWaitingToContinue = true;
            var g = new GameObject();
            DontDestroyOnLoad(g);
            var binding = g.AddComponent<CoroutineBinding>();
            binding.StartCoroutine(WaitForSceneChange(args[0].TrimToLower(), binding));
        }

        IEnumerator WaitForSceneChange(string sceneName, CoroutineBinding binding)
        {
            List<string> tags = story.currentTags;
            yield return _scenes[sceneName].LoadSceneAsync();
            //Remove the change scene event and parse the rest of the events, now that we are in the right scene.
            tags.RemoveAll(x => x.TrimToLower().Contains("changescene"));
            ParseTags(tags);
            FinishContinue();
            Destroy(binding.gameObject);
            isWaitingToContinue = false;
            _readyToContinueEvent?.Invoke();
        }

        void FinishChoice(Int32 choiceIndex)
        {
            story.ChooseChoiceIndex(choiceIndex);
            Continue();
        }
        
        void OnEnable()
        {
            hideFlags = HideFlags.DontUnloadUnusedAsset;
            isWaitingToContinue = false;
            _nextContinueJustShowsChoices = false;

            _waitEvent.OnRaised += WaitForSeconds;
            _readyToContinueEvent.OnRaised += Continue;
            _changeSceneEvent.OnRaised += OnSceneChangeEvent;

            foreach(var thing in _choiceTypesLibrary.Events)
            {
                thing.OnFinished += FinishChoice;
            }
        }
        

        void OnDisable()
        {
            _waitEvent.OnRaised -= WaitForSeconds;
            _readyToContinueEvent.OnRaised -= Continue;
            _changeSceneEvent.OnRaised -= OnSceneChangeEvent;

            foreach(var thing in _choiceTypesLibrary.Events)
            {
                thing.OnFinished -= FinishChoice;
            }
        }

        IEnumerator Wait(CoroutineBinding binding)
        {
            while(waitToContinueTimer > 0)
            {
                waitToContinueTimer -= Time.deltaTime;
                yield return null;
            }
            isWaitingToContinue = false;
            FinishContinue();
            Destroy(binding.gameObject);
        }
    }
}